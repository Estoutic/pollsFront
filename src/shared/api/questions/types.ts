export default interface IQuestion {
  questionId: string;
  description: string;
}
