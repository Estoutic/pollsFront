import { IAnswerType } from "../answers";
import { IQuestion } from "../questions";

export interface IPoll {
  pollId: string;
  state: boolean;
  categoryId: string;
  question: IQuestion;
  answers: IAnswerType[];
}

export type IPollType = "my" | "all";

export type FormFields = {
  question: string;
  answers: string[];
  category: string;
};
