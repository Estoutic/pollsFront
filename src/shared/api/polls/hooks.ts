import { UseMutationResult, useMutation, useQuery, useQueryClient } from "react-query";
import { pollApi } from "..";
import { FormFields, IPollType } from "./types"
import { AxiosError } from "axios";

export const fontPackKeys = {
  all: (pollType: IPollType) => ['polls', pollType],
  byId: (id?: string) => ['polls-by-id', id],
  create: (id? :string) => ['polls-create', id],
}
type GetParams = { pollType: IPollType };
type Params = FormFields;


export const usePolls = ({ pollType }: GetParams) => {
  const queryKey = fontPackKeys.all(pollType);
  const queryFn = () => pollApi.getAllPolls();

  return useQuery({ queryKey, queryFn, keepPreviousData:true, refetchOnWindowFocus:false});
};


export const useCreatePoll = (): UseMutationResult<string, AxiosError, Params> => {

  const mutationFn = async (data: Params) => {
    const pollId = await pollApi.createPoll(data)
    return pollId
  }

  return useMutation({ mutationFn })
}
