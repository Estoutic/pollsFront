import { AxiosInstance } from "axios";
import  { IPoll, FormFields } from "../polls";
import CustomApiClient from "../CustomApiClient";
import { ICategory } from "../category/types";

export interface IPollServiceApi {
  version: string;
  allCategories: string;
  pollsByCategory: (id: string) => string;
  allPolls: string;
  createPoll: string;
}

const routes: IPollServiceApi = {
  version: "api/application/version",
  allCategories: "api/category/all",
  pollsByCategory: (id: string) => `api/category/${id}`,
  allPolls: "api/poll/all",
  createPoll: "api/poll/create",
};

export class PollApi extends CustomApiClient<IPollServiceApi> {
  constructor(mainHttpClient: AxiosInstance) {
    super(mainHttpClient, routes);
  }

  version(): Promise<any> {
    return this.client
      .get<string>(this.methods.version)
      .then(({ data }) => data);
  }

  getAllPolls(): Promise<IPoll[]> {
    return this.client
      .get<IPoll[]>(this.methods.allPolls)
      .then(({ data }) => data);
  }

  getAllCategories(): Promise<ICategory[]> {
    return this.client
      .get<ICategory[]>(this.methods.allCategories)
      .then(({ data }) => data);
  }
  createPoll(formFileds: FormFields): Promise<string> {
    return this.client
      .post<string>(this.methods.createPoll, formFileds)
      .then(({ data }) => data);
  }
}
