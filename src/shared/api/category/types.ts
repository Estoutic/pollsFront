export interface ICategory {
    name: string;
    id: string;
    categoryType: string;
  }
  