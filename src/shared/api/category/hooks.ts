
import { useQuery } from "react-query";
import { pollApi } from "..";

export const KEYS = {
    categoryById: (id? : number) => ["category", id]
}

const useCategories = () => {
  const queryKey = KEYS.categoryById();
  const queryFn = () => pollApi.getAllCategories();

  return useQuery({ queryKey, queryFn, keepPreviousData:true, refetchOnWindowFocus:false});
};

export default useCategories;
