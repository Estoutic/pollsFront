import { useEffect, useState } from 'react'

function getStorageValue(key: string, defaultValue?: string) {
    const saved = localStorage.getItem(key)
    const initial = saved
    return initial || defaultValue
}

const useLocalStorage = (key: string, defaultValue?: string) => {
    const [value, setValue] = useState<string | undefined>(() => {
        return getStorageValue(key, defaultValue)
    })

    useEffect(() => {
        if (value) {
            localStorage.setItem(key, value)
        }
    }, [key, value])

    return [value, setValue]
}

export default useLocalStorage
