import { useEffect } from 'react'

import { useKeycloak } from '@react-keycloak/web'


import type { KeycloakTokenParsed } from 'keycloak-js'
import useLocalStorage from './useLocalStorage'

const useTokenInfo = (): KeycloakTokenParsed | null => {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    const { keycloak, initialized } = useKeycloak()
    const [profile, setProfile] = useLocalStorage('token', '')

    useEffect(() => {
        if (initialized) {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            setProfile(JSON.stringify(keycloak?.tokenParsed || ''))
        }
    }, [initialized])

    let res = null
    try {
        if (profile) {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            res = JSON.parse(profile)
        }
    } catch (error) {
        console.error('JSON profile parse error', error)
    }

    return res
}

export default useTokenInfo
