import { useMemo } from 'react'
import { KeycloakRoles } from '../service/keycloak'
import useCheckRoleAccessCallback from './useCheckRoleAccessCallback'



const useCheckRoleAccess = (roles: KeycloakRoles[]) => {
    const checkAccess = useCheckRoleAccessCallback()
    return useMemo(() => checkAccess(roles), [roles, checkAccess])
}

export default useCheckRoleAccess
