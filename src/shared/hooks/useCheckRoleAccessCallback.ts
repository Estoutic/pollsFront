import { useCallback } from 'react'
import { KeycloakRoles } from '../service/keycloak'

import useTokenInfo from './useTokenInfo'


const useCheckRoleAccessCallback = () => {
    const token = useTokenInfo()

    return useCallback(
        (roles?: KeycloakRoles[]) => {
            if (!roles || roles.length === 0) {
                return true
            }

            console.log(token);

            const keycloakRoles = token?.resource_access?.hw?.roles || []

            return roles.every((role) => keycloakRoles.includes(String(role)))
        },
        [token]
    )
}

export default useCheckRoleAccessCallback
