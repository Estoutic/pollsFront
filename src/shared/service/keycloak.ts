import Keycloak from 'keycloak-js'

const keycloak = new Keycloak({
    clientId: 'polls-rest-api',
    url: 'http://0.0.0.0:8080',
    realm: 'Estoutic',
})

export type KeycloakRoles =
    | 'admin'
    | 'user'

export default keycloak
