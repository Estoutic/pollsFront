import React, { FormEvent, useEffect, useState } from "react";
import { IBaseComponentProps } from "src/shared/types";
import "./PollCreateForm.css";
import {
  Button,
  FormItem,
  Group,
  Input,
  Panel,
  Select,
  View,
} from "@vkontakte/vkui";
import { SubmitHandler, useForm, Controller } from "react-hook-form";
import useCategories from "../../../shared/api/category";
import { FormFields } from "../../../shared/api/polls/types";
import { useCreatePoll } from "../../../shared/api/polls";

type Props = IBaseComponentProps;

const PollCreateForm = ({ className, ...rest }: Props) => {
  const classes = [className, "poll-form"];

  const {
    handleSubmit,
    control,
    formState: { errors, isSubmitting },
    setError,
    reset,
    setValue,
  } = useForm<FormFields>();

  const [answers, setAnswers] = useState<string[]>(["", ""]);
  const mutation = useCreatePoll();

  const onSubmit: SubmitHandler<FormFields> = async (data) => {
    try {
      console.log(data);
      mutation.mutate( data )
      reset();
      setValue("question", "");
      setValue("category", "");

    } catch (error) {
      setError("root", {
        message: "Опрос с таким вопросом уже сущесвтует",
      });
    }
  };

  //   Получение категорий с сервера

  const { data: categoriesData, isSuccess: isCategoriesSuccess } = useCategories();
  
  const categoryOptions = isCategoriesSuccess
    ? categoriesData.map((category) => ({
        value: category.id,
        label: category.name,
      }))
    : [];

  //  Функции для работы с овтетами

  const handleAddAnswer = () => {
    setAnswers([...answers, ""]);
    console.log(answers);
  };

  const onRemove = (index) => {
    if (index > 1) {
      const newAnswers = [...answers];
      newAnswers.splice(index, 1);
      console.log(newAnswers);
      setAnswers(newAnswers);
    }
  };

  return (
    <div className={classes.join(" ")}>
      <View activePanel="new-user">
        <Panel id="new-user">
          <Group>
            <form onSubmit={handleSubmit(onSubmit)}>
              <FormItem top="Вопрос" htmlFor="question">
                <Controller
                  name="question"
                  control={control}
                  render={({ field }) => (
                    <Input
                      {...field}
                      id="question"
                      placeholder="Вопрос опроса"
                    />
                  )}
                />
              </FormItem>
              {answers.map((answer, index) => (
                <FormItem
                  key={index}
                  top={`Ответ ${index + 1}`}
                  htmlFor={`answer-${index}`}
                  removable
                  onRemove={() => {
                    onRemove(index);
                  }}
                >
                  <Controller
                    name={`answers[${index}]` as any}
                    control={control}
                    defaultValue=""
                    render={({ field }) => (
                      <Input
                        {...field}
                        id={`answer-${index}`}
                        onChange={(e) => field.onChange(e.target.value)}
                        placeholder={`Ответ ${index + 1}`}
                      />
                    )}
                  />
                </FormItem>
              ))}

              <FormItem>
                <Button size="m" stretched onClick={handleAddAnswer}>
                  Добавить ответ
                </Button>
              </FormItem>

              <FormItem top="Категории" htmlFor="poll-category-id">
                <Controller
                  name="category"
                  control={control}
                  render={({ field }) => (
                    <Select
                      id="poll-category-id"
                      placeholder="Выберите категории"
                      options={categoryOptions}
                      {...field}
                      allowClearButton
                      emptyText="Возникли какие-то ошибки на сервере мы разберемся с этим позже..."
                    />
                  )}
                />
              </FormItem>

              <FormItem>
                <Button
                  size="l"
                  stretched
                  type="submit"
                  disabled={isSubmitting}
                >
                  {isSubmitting ? "Загрузка..." : "Создать"}
                </Button>
              </FormItem>
              {errors.root && <div>{errors.root.message}</div>}
            </form>
          </Group>
        </Panel>
      </View>
    </div>
  );
};

export default PollCreateForm;
