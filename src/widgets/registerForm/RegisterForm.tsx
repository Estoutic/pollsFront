import React from "react";
import { IBaseComponentProps } from "src/shared/types";
import "./RegisterForm.css"; // Assuming you have a CSS file for styling
import { Button, FormItem, Group, Input, Panel, View } from "@vkontakte/vkui";
import { SubmitHandler, useForm, Controller } from "react-hook-form";
import { IUserRegistration } from "src/shared/api/user";

type Props = IBaseComponentProps;

const RegisterForm = ({ className, ...rest }: Props) => {
  const classes = [className, "register"];

  const {
    handleSubmit,
    control,
    formState: { errors },
  } = useForm<IUserRegistration>();

  const onSubmit: SubmitHandler<IUserRegistration> = (data) => {
    console.log(data);
    // Handle form submission
  };

  return (
    <View activePanel="new-user" className={classes.join(" ")}>
      <Panel id="new-user">
        <Group>
          <form onSubmit={handleSubmit(onSubmit)}>
            <FormItem
              htmlFor="username"
              top="Имя пользователя"
            >
              <Controller
                name="username"
                control={control}
                defaultValue=""
                render={({ field }) => (
                  <Input
                    {...field}
                    id="username"
                    type="username"
                    placeholder="Введите ваше имя пользователя"
                  />
                )}
              />
            </FormItem>

            <FormItem htmlFor="password" top="Пароль">
              <Controller
                name="password"
                control={control}
                defaultValue=""
                render={({ field }) => (
                  <Input
                    {...field}
                    id="password"
                    type="password"
                    placeholder="Введите пароль"
                  />
                )}
              />
            </FormItem>
            <FormItem>
              <Button type="submit" size="l" stretched>
                Зарегистрироваться
              </Button>
            </FormItem>
          </form>
        </Group>
      </Panel>
    </View>
  );
};

export default RegisterForm;
