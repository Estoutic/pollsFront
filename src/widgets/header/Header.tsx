import React from "react";
import Categories from "../../features";
import { IBaseComponentProps } from "src/shared/types";
import "./Header.css";
import { Title } from "@vkontakte/vkui";
import { Link } from "react-router-dom";

type Props = IBaseComponentProps;

const Header = ({ className, ...rest }: Props) => {
  const classes = ["header", className].join(" ");
  return (
    <header className={classes}>
      <Link to="/">
        <Title level="1">Polls</Title>
      </Link>
      <Categories></Categories>
    </header>
  );
};

export default Header;
