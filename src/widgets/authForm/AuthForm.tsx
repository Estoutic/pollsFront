import { Button, CellButton, Checkbox, FormItem, FormLayoutGroup, Group, Input, Link, Panel, PanelHeader, SegmentedControl, Select, Textarea, View } from "@vkontakte/vkui";
import React from "react";
import { IBaseComponentProps } from "src/shared/types";
import "./AuthForm.css"

const addressItems = [
  { label: 'Почтовый индекс', name: 'zip' },
  { label: 'Страна', name: 'country' },
  { label: 'Город', name: 'city' },
];

type Props = IBaseComponentProps;

const AuthForm = ({className, ...rest} : Props) => {

  const classes = ["auth-form", className].join(" ")
  const [email, setEmail] = React.useState('');
  const [purpose, setPurpose] = React.useState('');
  const [showPatronymic, setShowPatronymic] = React.useState(true);

  const onChange = (e) => {
    const { name, value } = e.currentTarget;

    const setStateAction = {
      email: setEmail,
      purpose: setPurpose,
    }[name];

    setStateAction && setStateAction(value);
  };

  return (
    <View activePanel="new-user" className = {classes}>
      <Panel id="new-user">
        <Group >
          <form onSubmit={(e) => e.preventDefault()}>
            
            <FormItem
              htmlFor="email"
              top="Имя пользователя"
              status={email ? 'valid' : 'error'}
              bottom={
                email ? 'Электронная почта введена верно!' : 'Пожалуйста, введите имя пользователя'
              }
              bottomId="email-type"
            >
              <Input
                aria-labelledby="email-type"
                id="email"
                type="email"
                name="email"
                value={email}
                required
                onChange={onChange}
              />
            </FormItem>

            <FormItem top="Пароль" htmlFor="pass">
              <Input id="pass" type="password" placeholder="Введите пароль" />
            </FormItem>
            <FormItem>
              <Button type="submit" size="l" stretched>
                Зарегистрироваться
              </Button>
            </FormItem>
          </form>
        </Group>
      </Panel>
    </View>
  );
};

export default AuthForm;