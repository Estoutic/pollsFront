import { ReactKeycloakProvider } from "@react-keycloak/web";
import React from "react";
import { QueryClient, QueryClientProvider } from "react-query";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import keycloak from "../shared/service/keycloak";
import { Feed, CreatePoll, ErrorPage, AuthPage } from "../pages";

function App() {
  const queryClient = new QueryClient();

  const router = createBrowserRouter([
    {
      path: "/",
      element: <Feed />,
      errorElement: <ErrorPage />,
    },
    {
      path: "/create",
      element: <CreatePoll />,
    },
    {
      path: "/login",
      element: <AuthPage />
    },
  ]);

  return (
    <>
      <ReactKeycloakProvider
        authClient={keycloak}
        initOptions={{
          onLoad: 'check-sso',
          silentCheckSsoRedirectUri: window.location.origin,
        }}>
        <QueryClientProvider client={queryClient}>
          <RouterProvider router={router} />
        </QueryClientProvider>
      </ReactKeycloakProvider>
    </>
  );
}

export default App;
