import React from "react";
import useCheckRoleAccess from "../../../shared/hooks/useCheckRoleAccess";
import { IBaseComponentProps } from "src/shared/types";
import { Header, Polls } from "../../../widgets";
import "./Feed.css";

type Props = IBaseComponentProps;

const Feed = ({ className, ...rest }: Props) => {
  const classes = ["feed", className].join(" ");
  const hasAccess = useCheckRoleAccess(['dev'])
  return (
    <div className={classes}>
      <h1>{hasAccess}</h1>
      <Header></Header>
      <Polls></Polls>
    </div>
  );
};

export default Feed;
