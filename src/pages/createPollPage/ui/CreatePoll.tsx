import React, { FormEvent, useState } from "react";
import { IBaseComponentProps } from "src/shared/types";
import "./CreatePoll.css";
import { Image, Title } from "@vkontakte/vkui";
import { PollCreateForm } from "../../..//widgets";
import { Link } from "react-router-dom";

type Props = IBaseComponentProps;

const CreatePoll = ({ className, ...rest }: Props) => {
  const classes = [className, "create-poll"];

  const size = 40;

  return (
    <div className={classes.join(" ")}>
      <div className="top-container">
        <Link to="/">
          <Image
            key={size}
            size={size}
            src="./arrow.svg"
            borderRadius="l"
          ></Image>
        </Link>
        <Title>Создание опроса</Title>
      </div>
      <PollCreateForm />
    </div>
  );
};

export default CreatePoll;
