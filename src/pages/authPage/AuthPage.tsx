import { Card, CardGrid } from "@vkontakte/vkui";
import React from "react";
import { IBaseComponentProps } from "src/shared/types";
import { AuthForm, RegisterForm } from "../../widgets";
import "./AuthPage.css"
type Props = IBaseComponentProps;

const AuthPage = ({className, ...rest} : Props) => {

    const classes = ["", className].join(" ")
    return ( 
        <div className="login-layout">
            <div className="login-container"> 
                    <RegisterForm />
                    <AuthForm />
            </div>
        </div>
    )
}

export default AuthPage;